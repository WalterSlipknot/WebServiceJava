package ejercicios;

import java.util.Scanner;

/**
 *
 * @author anonimo
 */
public class Ejercicios {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Ejercicio N° 2");
        System.out.println("---------------");
        System.out.println("Hola Mundo!!");
        System.out.println(" ");
        
        System.out.println("Ejercicio N° 3");
        System.out.println("---------------");
        float num1 = 12;
        float num2 = 5;
        
        System.out.println("Suma: " + (num1 + num2));
        System.out.println("Resta: " + (num1 - num2));
        System.out.println("Multipicación: " + (num1 * num2));
        System.out.println("División: " + (num1 / num2));
        System.out.println("Módulo: " + (num1%num2));
        System.out.println(" ");
        
        System.out.println("Ejercicio N° 4");
        System.out.println("---------------");
        int num3 = 5;
        int num4 = 5;
        
        if (num3 > num4)
            {
                System.out.println("La variable num3 de valor " + num3 + " es mayor a num4 de valor " + num4);
            }
            else if(num3 == num4)
            {
                System.out.println("La variable num3 de valor " + num3 + " es igual a num4 de valor " + num4);
            }
        else
            {
                System.out.println("La variable num3 de valor " + num3 + " es menor a num4 de valor " + num4);
            };
            
        System.out.println(" ");
        System.out.println("Ejercicio N° 5");
        System.out.println("---------------");
        System.out.println("Numeros primos: ");
        for (int i=1;i<1001;i++){
            int varPrimos = -2;
            for (int j=1;j<=i;j++){
                if ((i%j)== 0){
                        varPrimos++;
                }
            }
            if (varPrimos == 0 || i == 1){
                System.out.print(i + ", ");
            }
        }
        System.out.println("");
        System.out.println("Años bisiestos: ");
        int varBisiesto = 0;
        for (int i=1997;i<3001;i++){
            varBisiesto++;
            if (varBisiesto == 4){
                System.out.print(i + ", ");
                varBisiesto = 0;
            }
        }
        System.out.println("");
        System.out.println("Ejercicio N° 6");
        System.out.println("---------------");
        int sumaFactorial = 1;
        int varFactorial = 6;
        for (int i=1;i<=varFactorial;i++){
            sumaFactorial *= i; 
        }
        System.out.println("Factorial: " + sumaFactorial);
        System.out.println("");
        System.out.println("Ejercicio N° 7");
        System.out.println("---------------");
        int suma = 0;
        for (int i=1;i<=100;i++){
            suma += i; 
        }
        System.out.println("Suma: " + suma);
        System.out.println("");
        System.out.println("Ejercicio N° 8");
        System.out.println("---------------");
        int arreglo[] = new int[5];
        arreglo[0] = 4;
        arreglo[1] = 5;
        arreglo[2] = 7;
        arreglo[3] = 45;
        arreglo[4] = 89;
        for (int i=0;i<5;i++){
            System.out.println("Posición : " + i + ", valor: " + arreglo[i]); 
        }
        System.out.println("");
        System.out.println("Ejercicio N° 9");
        System.out.println("---------------");
        int arregloConsola[] = new int[5];
        Scanner teclado = new Scanner (System.in);
        for(int i=0;i<5;i++){
            System.out.println ("Escriba el numero para la poscición: " + i);
            int rango1 = teclado.nextInt();
            arregloConsola[i] = rango1;
        }
        for (int i=0;i<5;i++){
            System.out.println("Posición : " + i + ", valor: " + arregloConsola[i]); 
        }
        System.out.println("");
        System.out.println("Ejercicio N° 10");
        System.out.println("---------------");
        System.out.println ("Escriba el tamaño del arreglo");
        int rango1 = teclado.nextInt();
        int arregloDinamico[] = new int[rango1];
        multiplos(arregloDinamico);
        visualizarArreglo(arregloDinamico);
    }
    
    public static void multiplos(int[] arregloDinamico){
        Scanner teclado = new Scanner (System.in);
        System.out.println ("Escriba el número para obtener sus múltiplos");
        int rango2 = teclado.nextInt();
        for(int i=0;i<arregloDinamico.length;i++){
            arregloDinamico[i] = rango2 * (i+1);
        }
    }
    
    public static void visualizarArreglo(int[] arregloDinamico){
        for (int i=0;i<arregloDinamico.length;i++){
            System.out.println("Posición : " + i + ", valor: " + arregloDinamico[i]); 
        }
    }
    
    }



   

